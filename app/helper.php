<?php 

function notif($alert='success', $message='') {
	return [
		'alert' => $alert,
		'message' => $message,
	];
}

function slug($text='')
{
	return \Str::slug($text);
}

function storeFile($pathFolder='', $file=null, $oldFile=null)
{
	$fileName = $file->getClientOriginalName();
	$name = pathinfo($fileName, PATHINFO_FILENAME);
	$ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $path = $pathFolder .'/'. $fileName;

    $exists = \Storage::disk('public')->exists($path);

    if ($exists) {
        $i = 2;
        while(\Storage::disk('public')->exists(
            $pathFolder.'/'.$name.'_'.$i.'.'.$ext)
        ) $i++;
        
        $fileName = $name.'_'.$i.'.'.$ext;
    }

    if ($oldFile) {
        deleteFile($pathFolder, $oldFile);
    }

    \Storage::disk('public')->putFileAs(
    	$pathFolder,
    	$file,
    	$fileName,
    	'public'
    );

    return $fileName;
}

function deleteFile($pathFolder='', $fileName=null)
{
    $path = $pathFolder .'/'. $fileName;
    $exists = \Storage::disk('public')->exists($path);

    if ($exists) {
        \Storage::disk('public')->delete($pathFolder .'/'. $fileName);   
    }
}