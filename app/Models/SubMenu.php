<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    protected $table = 'sub_menus';

    protected $guarded = [
        'id'
    ];

    public function forms()
    {
        return $this->hasMany(Form::class, 'sub_menu_id');
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }
}
