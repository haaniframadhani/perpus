<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';

    protected $guarded = [
        'id'
    ];

    public function landasanHukum()
    {
        return $this->hasMany(LandasanHukum::class, 'menu_id')
                    ->orderBy('year', 'desc');
    }

    public function subMenu()
    {
        return $this->hasMany(SubMenu::class, 'menu_id');
    }
}
