<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = 'forms';

    protected $guarded = [
        'id'
    ];

    public function subMenu()
    {
        return $this->belongsTo(SubMenu::class, 'sub_menu_id');
    }

    public function getFileUrlAttribute()
    {
        $url = '#';

        if (\Storage::disk('public')->exists('formulir/' . $this->file)) {
            $url = \Storage::url('formulir/'. $this->file);
        }

        return $url;
    }
}
