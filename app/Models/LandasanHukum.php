<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandasanHukum extends Model
{
    protected $table = 'landasan_hukum';

    protected $guarded = [
        'id'
    ];

    public function getFileUrlAttribute()
    {
        $url = '#';

        if (\Storage::disk('public')->exists('landasan-hukum/' . $this->file)) {
            $url = \Storage::url('landasan-hukum/'. $this->file);
        }

        return $url;
    }
}
