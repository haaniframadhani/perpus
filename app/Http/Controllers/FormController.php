<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use App\Models\SubMenu;

class FormController extends Controller
{

    public function create($slug, $slug2)
    {
        $subMenu = SubMenu::where('slug',$slug2)
                    ->with([
                        'menu'
                    ])
                    ->first();

        if (is_null($subMenu)) {
            abort(404);
        }

        return view('formulir.create', compact('slug', 'slug2', 'subMenu'));
    }


    public function store(Request $request, $slug, $slug2)
    {
        $request->validate([
            'name' => 'required',
            'document' => 'required'
        ]);

        try {
            $subMenu = SubMenu::where('slug', $slug2)->first();

            $pathFolder = 'formulir';
            $fileName = storeFile($pathFolder, $request->file('document'));

            Form::create([
                'sub_menu_id' => $subMenu->id,
                'kode' => $request->kode,
                'name' => $request->name,
                'is_allowed' => $request->is_allowed ? 1 : 0,
                'file' => $fileName,
            ]);
            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->withInput()->with($notif);
        }

        return redirect()->route('menu.sub-menu.list', ['slug' => $slug, 'slug2' => $slug2])->with($notif);
    }


    public function edit($slug, $slug2, $id)
    {
        $subMenu = SubMenu::where('slug',$slug2)
                    ->with([
                        'menu'
                    ])
                    ->first();
        $data = Form::findOrFail($id);

        if (is_null($subMenu)) {
            abort(404);
        }
        
        return view('formulir.edit', compact('slug', 'slug2', 'data', 'subMenu'));
    }


    public function update(Request $request, $slug, $slug2, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        try {
            $subMenu = SubMenu::where('slug', $slug2)->first();

            $pathFolder = 'formulir';
            $fileName = $request->old_file;
            if ($request->hasFile('document')) {
                $fileName = storeFile($pathFolder, $request->file('document'), $request->old_file);
            }

            Form::where('id', $id)->update([
                'sub_menu_id' => $subMenu->id,
                'kode' => $request->kode,
                'name' => $request->name,
                'is_allowed' => $request->is_allowed ? 1 : 0,
                'file' => $fileName,
            ]);
            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->withInput()->with($notif);
        }

        return redirect()->route('menu.sub-menu.list', ['slug' => $slug, 'slug2' => $slug2])->with($notif);
    }


    public function delete($slug, $slug2, $id)
    {
        try {
            $data = Form::findOrFail($id);

            $pathFolder = 'formulir';
            deleteFile($pathFolder, $data->file);

            $data->delete();
            
            $notif = notif('success', 'Data berhasil dihapus.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
        }

        return redirect()->back()->with($notif);
    }
}
