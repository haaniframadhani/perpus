<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\SubMenu;

class ResourceController extends Controller
{
	public function menu(Request $request)
	{
		$query = Menu::query();

        if ($request->has('q')) {
            $query->where('name', 'like', '%'.$request->q.'%');
        }

        $menu = $query->limit(10)->get();

        $data = [];
        foreach ($menu as $key => $row) {
            $data[$row->id]['id'] = $row->id;
            $data[$row->id]['text'] = $row->name;
        }

        $data = array_values($data);
        $data = ['results' => $data];

        return response()->json($data);
	}

	public function subMenu(Request $request)
	{
		$query = SubMenu::query();

        if ($request->has('menu_id')) {
            $query->where('menu_id', $request['menu_id']);
        }

        if ($request->has('q')) {
            $query->where('name', 'like', '%'.$request['q'].'%');
        }

        $menu = $query->limit(10)->get();

        $data = [];
        foreach ($menu as $key => $row) {
            $data[$row->id]['id'] = $row->id;
            $data[$row->id]['text'] = $row->name;
        }

        $data = array_values($data);
        $data = ['results' => $data];

        return response()->json($data);
	}
}