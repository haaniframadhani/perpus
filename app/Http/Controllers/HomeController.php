<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\SubMenu;

class HomeController extends Controller
{
    public function home()
    {
        $data = Menu::get();
        return view('home', compact('data'));
    }

    public function list($slug)
    {
        $menu = Menu::where('slug', $slug)->with([
                        'landasanHukum',
                        'subMenu',
                    ])
                    ->first();

        if (is_null($menu)) {
            abort(404);
        }

        return view('list', compact('menu'));
    }

    public function formulir($slug='', $slug2)
    {
        $subMenu = SubMenu::where('slug', $slug2)->with([
                        'forms',
                        'menu'
                    ])
                    ->first();

        if (is_null($subMenu)) {
            abort(404);
        }
                    
        return view('list-formulir', compact('subMenu'));   
    }
}
