<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;

class FormUmumController extends Controller
{

    public function index()
    {
        // $data = Form::whereNull('sub_menu_id')->get();
        $data = Form::get();
        return view('form-umum.index', compact('data'));
    }

    // for guest 
    public function list()
    {
        $data = Form::get();
        return view('form-umum.list-for-guest', compact('data'));
    }

    public function create()
    {
        return view('form-umum.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'document' => 'required'
        ]);

        try {
            $pathFolder = 'formulir';
            $fileName = storeFile($pathFolder, $request->file('document'));

            Form::create([
                'sub_menu_id' => $request->sub_menu,
                'kode' => $request->kode,
                'name' => $request->name,
                'is_allowed' => $request->is_allowed ? 1 : 0,
                'file' => $fileName,
            ]);
            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->withInput()->with($notif);
        }

        return redirect()->route('formulir.index')->with($notif);
    }


    public function edit($id)
    {
        $data = Form::findOrFail($id);
        return view('form-umum.edit', compact('data'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        try {
            $pathFolder = 'formulir';
            $fileName = $request->old_file;
            if ($request->hasFile('document')) {
                $fileName = storeFile($pathFolder, $request->file('document'), $request->old_file);
            }

            Form::where('id', $id)->update([
                'sub_menu_id' => $request->sub_menu,
                'kode' => $request->kode,
                'name' => $request->name,
                'is_allowed' => $request->is_allowed ? 1 : 0,
                'file' => $fileName,
            ]);
            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->withInput()->with($notif);
        }

        return redirect()->route('formulir.index')->with($notif);
    }


    public function delete($id)
    {
        try {
            $data = Form::findOrFail($id);

            $pathFolder = 'formulir';
            deleteFile($pathFolder, $data->file);

            $data->delete();
            
            $notif = notif('success', 'Data berhasil dihapus.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
        }

        return redirect()->back()->with($notif);
    }
}
