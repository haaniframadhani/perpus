<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SubMenu;
use App\Models\Menu;

class SubMenuController extends Controller
{
    public function create($slug)
    {
        $menu = Menu::where('slug', $slug)->first();

        if (is_null($menu)) {
            abort(404);
        }

        return view('sub-menu.create', compact('slug', 'menu'));
    }


    public function store(Request $request, $slug)
    {
        $request->validate([
            'name' => 'required'
        ]);

        try {

            $menu = Menu::where('slug', $slug)->first();

            SubMenu::create([
                'menu_id' => $menu->id,
                'name' => $request->name,
                'slug' => slug($request->name),
            ]);

            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->with($notif);
        }

        return redirect()->route('menu.list', $slug)->with($notif);
    }



    public function edit($slug='', $id)
    {
        $menu = Menu::where('slug', $slug)->first();
        $data = SubMenu::findOrFail($id);

        if (is_null($menu)) {
            abort(404);
        }
        
        return view('sub-menu.edit', compact('slug', 'data', 'menu'));
    }


    public function update(Request $request, $slug='', $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        try {
            $menu = Menu::where('slug', $slug)->first();

            SubMenu::where('id', $id)
                ->update([
                    'menu_id' => $menu->id,
                    'name' => $request->name,
                    'slug' => slug($request->name),
                ]);

            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->with($notif);
        }

        return redirect()->route('menu.list', $slug)->with($notif);
    }


    public function delete($slug='', $id)
    {
        try {
            $subMenu = SubMenu::findOrFail($id);

            if ($subMenu->forms->count() > 0) {
                $notif = notif('error', 'Gagal! Data ini masih memiliki formulir.');
                return redirect()->back()->with($notif);
            }

            $subMenu->delete();

            $notif = notif('success', 'Data berhasil dihapus.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
        }

        return redirect()->back()->with($notif);
    }


    public function list($slug='', $slug2)
    {
        $subMenu = SubMenu::where('slug',$slug2)
                    ->with([
                        'forms',
                        'menu'
                    ])
                    ->first();

        return view('sub-menu.list', compact('subMenu', 'slug'));   
    }
}
