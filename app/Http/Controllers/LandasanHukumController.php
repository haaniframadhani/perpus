<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LandasanHukum;
use App\Models\Menu;

class LandasanHukumController extends Controller
{

    public function create($slug='')
    {
        $menu = Menu::where('slug', $slug)->first();

        if (is_null($menu)) {
            abort(404);
        }

        return view('landasan-hukum.create', compact('menu'));
    }


    public function store(Request $request, $slug='')
    {
        $request->validate([
            'year' => 'required',
            'name' => 'required',
            'document' => 'required'
        ]);

        try {
            $menu = Menu::where('slug', $slug)->first();

            $pathFolder = 'landasan-hukum';
            $fileName = storeFile($pathFolder, $request->file('document'));

            LandasanHukum::create([
                'menu_id' => $menu->id,
                'year' => $request->year,
                'name' => $request->name,
                'file' => $fileName,
            ]);
            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->with($notif);
        }

        return redirect()->route('menu.list', $slug)->with($notif);
    }


    public function edit($slug='', $id)
    {
        $menu = Menu::where('slug', $slug)->first();
        $data = LandasanHukum::findOrFail($id);

        if (is_null($menu)) {
            abort(404);
        }
        
        return view('landasan-hukum.edit', compact('slug', 'data', 'menu'));
    }


    public function update(Request $request, $slug='', $id)
    {
        $request->validate([
            'year' => 'required',
            'name' => 'required',
        ]);

        try {
            $menu = Menu::where('slug', $slug)->first();

            $pathFolder = 'landasan-hukum';
            $fileName = $request->old_file;
            if ($request->hasFile('document')) {
                $fileName = storeFile($pathFolder, $request->file('document'), $request->old_file);
            }

            LandasanHukum::where('id', $id)
                ->update([
                    'menu_id' => $menu->id,
                    'year' => $request->year,
                    'name' => $request->name,
                    'file' => $fileName,
                ]);
            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->with($notif);
        }

        return redirect()->route('menu.list', $slug)->with($notif);
    }

    public function delete($slug='', $id)
    {
        try {
            $data = LandasanHukum::findOrFail($id);

            $pathFolder = 'landasan-hukum';
            deleteFile($pathFolder, $data->file);

            $data->delete();

            $notif = notif('success', 'Data berhasil dihapus.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
        }

        return redirect()->back()->with($notif);
    }
}
