<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{
    public function index($value='')
    {
        $data = Menu::get();
        return view('menu.index', compact('data'));
    }


    public function create()
    {
        return view('menu.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        try {
            Menu::create([
                'name' => $request->name,
                'slug' => slug($request->name),
                'description' => $request->description,
            ]);
            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->with($notif);
        }

        return redirect()->route('menu.index')->with($notif);
    }


    public function edit($id)
    {   
        $data = Menu::findOrFail($id);
        return view('menu.edit', compact('data'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        try {
            Menu::where('id', $id)->update([
                'name' => $request->name,
                'slug' => slug($request->name),
                'description' => $request->description
            ]);
            $notif = notif('success', 'Data berhasil disimpan.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
            return redirect()->back()->with($notif);
        }

        return redirect()->route('menu.index')->with($notif);
    }

    public function delete($id)
    {
        try {
            $menu = Menu::findOrFail($id);

            if ($menu->subMenu->count() > 0) {
                $notif = notif('error', 'Gagal! Data ini masih memiliki sub menu.');
                return redirect()->back()->with($notif);
            }

            if ($menu->landasanHukum->count() > 0) {
                $notif = notif('error', 'Gagal! Data ini masih memiliki data landasan hukum.');
                return redirect()->back()->with($notif);
            }

            $menu->delete();

            $notif = notif('success', 'Data berhasil dihapus.');
        } catch (\Exception $e) {
            $notif = notif('error', $e->getMessage());
        }

        return redirect()->back()->with($notif);
    }


    public function list($slug='')
    {
        $menu = Menu::where('slug',$slug)
                    ->with([
                        'landasanHukum',
                        'subMenu',
                    ])
                    ->first();
                    
        return view('menu.list', compact('menu'));   
    }
}
