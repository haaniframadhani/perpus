function customSelect2WithParameter(placeholder, object, parameters=[], url) {
    object.select2({
        theme: 'bootstrap4',
        placeholder: placeholder,
        allowClear:true,
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            data: function(params){
                var query = parameters
                query.q = params.term
                return query
            },
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    });
}

function customSelect2(placeholder, object, url) {
    object.select2({
        theme: 'bootstrap4',
        placeholder: placeholder,
        allowClear:true,
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            success: function (data) {
                return data;
            },
            cache: true
        }
    });
}