@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row mt-2">
        <div class="col-sm-12">
          <h3 class="m-0 font-weight-bold">Dokumen Sistem Penjaminan Mutu Internal (SPMI)</h3>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="row my-3">
        <div class="col-lg col-md-12">
          <div class="card small-9">
            <div class="card-body table-responsive">
              <div class="row">
                <div class="col-lg col-md-12">
                  <table class="table table-hover text-nowrap table-bordered">
                    <thead>
                      <tr>
                        <th width="50px">No.</th>
                        <th>Nama Dokumen</th>
                        <th>Deskripsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @forelse ($data as $item)
                        <tr class="align-middle">
                          <th class="text-center">
                            {{ $loop->iteration }}
                          </th>
                          <td>
                              <a href="{{ route('list', $item->slug) }}">{{ $item->name ?? '-' }}</a>
                          </td>
                          <td>{{ $item->description ?? '-' }}</td>
                        </tr>
                      @empty
                        <tr>
                          <td colspan="3" class="text-center">Belum Ada Data</td>
                        </tr>
                      @endforelse
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
@endsection