@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row mt-2">
        <div class="col-sm-12">
          <a href="{{ route('menu.list', $subMenu->menu->slug) }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h4 class="m-0 text-dark">
            {{ $subMenu->name ?? '-' }}
          </h4>
        </div><!-- /.col -->
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('menu.list', $subMenu->menu->slug) }}">{{ $subMenu->menu->name }}</a></li>
            <li class="breadcrumb-item active">{{ $subMenu->name }}</li>
          </ol>
        </div>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="row my-3">
      <div class="col-lg col-md-12">
        <div class="card card-primary card-outline shadow">
          <div class="card-header">
            <div class="card-title">Formulir</div>
          </div>
          <div class="card-body table-responsive">
            <div class="row mb-3">
              <div class="col-md-12">
                <a href="{{ route('menu.sub-menu.formulir.create', ['slug' => $slug, 'slug2' => $subMenu->slug]) }}" class="btn btn-primary text-white"><i class="fas fa-plus"></i> Tambah</a>
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md-12">
                <table class="table table-hover text-nowrap table-bordered">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Nomor/Kode Formulir</th>
                      <th>Nama</th>
                      <th>Izinkan Download</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($subMenu->forms as $item)
                      <tr class="align-middle">
                        <td class="text-center col-2">
                          <div class="btn-group transparent">
                            <a data-method="delete" data-confirm="Anda yakin ingin menghapus data ini?" href="{{ route('menu.sub-menu.formulir.delete', ['slug' => $slug, 'slug2' => $subMenu->slug, 'id' => $item->id]) }}"class="btn btn-sm btn-primary custom-hover" title="Hapus"><i class="fas fa-fw fa-trash"></i></a>
                            <a href="{{ route('menu.sub-menu.formulir.edit', ['slug' => $slug, 'slug2' => $subMenu->slug, 'id'=>$item->id]) }}" class="btn btn-sm btn-primary custom-hover" title="Edit"><i class="fas fa-fw fa-edit"></i></a>
                          </div>
                        </td>
                        <td>{{ $item->kode ?? '-' }}</td>
                        <td>{{ $item->name ?? '-' }}</td>
                        <td>{{ $item->is_allowed ? 'Ya' : 'Tidak' }}</td>
                      </tr>
                    @empty
                      <tr>
                        <td colspan="4" class="text-center">Belum Ada Data</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection