@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row mt-2">
        <div class="col-sm-12">
          <a href="{{ route('list', $subMenu->menu->slug) }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h4 class="m-0 text-dark">
            {{ $subMenu->name ?? '-' }}
          </h4>
        </div><!-- /.col -->
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('list', $subMenu->menu->slug) }}">{{ $subMenu->menu->name }}</a></li>
            <li class="breadcrumb-item active">{{ $subMenu->name }}</li>
          </ol>
        </div>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="row my-3">
      <div class="col-lg col-md-12">
        <div class="card card-primary card-outline shadow">
          <div class="card-header">
            <div class="card-title">Formulir</div>
          </div>
          <div class="card-body table-responsive">
            <div class="row">
              <div class="col-lg col-md-12">
                <table class="table table-hover text-nowrap table-bordered" id="dashboard">
                  <thead>
                    <tr>
                      {{-- <th width="50px">No.</th> --}}
                      <th>Nomor/Kode Formulir</th>
                      <th>Nama</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($subMenu->forms as $item)
                      <tr class="align-middle">{{-- 
                        <td class="text-center">
                          {{ $loop->iteration }}
                        </td> --}}
                        <td>{{ $item->kode ?? '-' }}</td>
                        <td>

                          @if ($item->is_allowed)
                            <a href="{{ $item->file_url }}" target="_blank">{{ $item->name ?? '-' }}</a>
                          @else
                            {{ $item->name ?? '-' }}
                          @endif
                        </td>
                      </tr>
                    @empty
                      <tr>
                        <td colspan="4" class="text-center">Belum Ada Data</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection


@section('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('scripts')
<!-- DataTables -->
  <script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script>
      $("#dashboard").DataTable({
        "responsive": false,
        "autoWidth": true,
      });
  </script>
@endsection