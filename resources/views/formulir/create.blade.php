@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row my-2">
        <div class="col-sm-12">
          <a href="{{ route('menu.sub-menu.list', ['slug' => $subMenu->menu->slug, 'slug2' => $subMenu->slug]) }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h4 class="m-0 text-dark">
            Tambah Formulir
          </h4>
        </div><!-- /.col -->
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('menu.list', $subMenu->menu->slug) }}">{{ $subMenu->menu->name }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('menu.sub-menu.list', ['slug' => $subMenu->menu->slug, 'slug2' => $subMenu->slug]) }}">{{ $subMenu->name }}</a></li>
            <li class="breadcrumb-item active">Formulir</li>
          </ol>
        </div>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  <!-- Main content -->
    <div class="content">
      <div class="row">
        <!-- /.col-md-6 -->
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post" action="{{ route('menu.sub-menu.formulir.store', ['slug'=>$slug, 'slug2'=>$slug2]) }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="">Nomor</label>
                  <input type="text" name="kode" id="" class="form-control form-control-sm" placeholder="Input nomor formulir" value="{{ old('kode') }}" required autofocus>
                  @error('kode')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Nama Formulir</label>
                  <input type="text" name="name" id="" class="form-control form-control-sm" placeholder="Input nama formulir" value="{{ old('name') }}" required>
                  @error('name')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Dokumen</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" name="document" class="custom-file-input" id="exampleInputFile" required>
                      <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                    </div>
                  </div>
                  @error('document')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label>Izinkan Download?</label>
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="is_allowed" class="custom-control-input is-allowed" id="customSwitch1">
                    <label class="custom-control-label is-allowed-label" for="customSwitch1"></label>
                  </div>
                </div>

                <div class="form-group row">
                  <button class="btn btn-block btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init();

      $(".is-allowed").on('change', function() {
        var checked = $(this).is(":checked");

        if (checked) {
          $('.is-allowed-label').text('Ya');
        } else {
          $('.is-allowed-label').text('Tidak');
        }
      }).trigger('change');

    });
  </script>
@endsection