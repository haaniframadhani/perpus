@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row mt-2">
        <div class="col-sm-12">
          <a href="{{ route('menu.index') }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h3 class="m-0 text-dark">
            {{ $menu->name ?? '-' }}
          </h3>
        </div><!-- /.col -->
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></li>
            <li class="breadcrumb-item active">{{ $menu->name }}</li>
          </ol>
        </div>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="row my-3">
      <div class="col-lg col-md-12">
        <div class="card card-primary card-outline shadow">
          <div class="card-header">
            <div class="card-title">Landasan Hukum</div>
          </div>
          <div class="card-body table-responsive">
            <div class="row mb-3">
              <div class="col-md-12">
                <a href="{{ route('menu.landasan-hukum.create', $menu->slug) }}" class="btn btn-primary text-white"><i class="fas fa-plus"></i> Tambah</a>
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md-12">
                <table class="table table-hover text-nowrap table-bordered">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Tahun</th>
                      <th>Dasar Hukum</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($menu->landasanHukum as $item)
                      <tr class="align-middle">
                        <td class="text-center col-2">
                          <div class="btn-group transparent">
                            <a data-method="delete" data-confirm="Anda yakin ingin menghapus data ini?" href="{{ route('menu.landasan-hukum.delete', ['slug' => $menu->slug, 'id' => $item->id]) }}"class="btn btn-sm btn-primary custom-hover" title="Hapus"><i class="fas fa-fw fa-trash"></i></a>
                            <a href="{{ route('menu.landasan-hukum.edit', ['slug' => $menu->slug, 'id' => $item->id]) }}" class="btn btn-sm btn-primary custom-hover" title="Edit"><i class="fas fa-fw fa-edit"></i></a>
                          </div>
                        </td>
                        <td>{{ $item->year ?? '-' }}</td>
                        <td>{{ $item->name ?? '-' }}</td>
                      </tr>
                    @empty
                      <tr>
                        <td colspan="3" class="text-center">Belum Ada Data</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="row my-3">
      <div class="col-lg col-md-12">
        <div class="card card-primary card-outline shadow">
          <div class="card-header">
            <div class="card-title">
              Sub Menu
            </div>
          </div>
          <div class="card-body table-responsive">
            <div class="row mb-3">
              <div class="col-md-12">
                <a href="{{ route('menu.sub-menu.create', $menu->slug) }}" class="btn btn-primary text-white"><i class="fas fa-plus"></i> Tambah</a>
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md-12">
                <table class="table table-hover text-nowrap table-bordered">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Dokumen</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($menu->subMenu as $item)
                      <tr class="align-middle">
                        <td class="text-center col-2">
                          <div class="btn-group transparent">
                            <a data-method="delete" data-confirm="Anda yakin ingin menghapus data ini?" href="{{ route('menu.sub-menu.delete', ['slug' => $menu->slug, 'id' => $item->id]) }}"class="btn btn-sm btn-primary custom-hover" title="Hapus"><i class="fas fa-fw fa-trash"></i></a>
                            <a href="{{ route('menu.sub-menu.edit', ['slug' => $menu->slug, 'id' => $item->id]) }}" class="btn btn-sm btn-primary custom-hover" title="Edit"><i class="fas fa-fw fa-edit"></i></a>
                            <a href="{{ route('menu.sub-menu.list', ['slug' => $menu->slug, 'slug2' => $item->slug]) }}" class="btn btn-sm btn-primary custom-hover" title="List"><i class="fas fa-fw fa-list"></i></a>
                          </div>
                        </td>
                        <td>{{ $item->name ?? '-' }}</td>
                      </tr>
                    @empty
                      <tr>
                        <td colspan="3" class="text-center">Belum Ada Data</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection