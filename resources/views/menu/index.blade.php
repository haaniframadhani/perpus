@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row mt-2">
        <div class="col-sm-6">
          <h3 class="m-0 text-dark">Dokumen Sistem Penjamin Mutu Internal</h3>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="row my-3">
        <div class="col-lg col-md-12">
          <div class="card card-primary card-outline shadow">
            <div class="card-body table-responsive">
              <div class="row my-3">
                <div class="col-md-12">
                  <a href="{{ route('menu.create') }}" class="btn btn-primary text-white"><i class="fas fa-plus"></i> Tambah</a>
                </div>
              </div>
              <div class="row">
                <div class="col-lg col-md-12">
                  <table class="table table-hover text-nowrap table-bordered">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Nama Dokumen</th>
                        <th>Deskripsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @forelse ($data as $item)
                        <tr class="align-middle">
                          <td class="text-center col-2">
                            <div class="btn-group transparent">
                              <a data-method="delete" data-confirm="Anda yakin ingin menghapus data ini?" href="{{ route('menu.delete', $item->id) }}"class="btn btn-sm btn-primary custom-hover" title="Hapus"><i class="fas fa-fw fa-trash"></i></a>
                              <a href="{{ route('menu.edit', $item->id) }}" class="btn btn-sm btn-primary custom-hover" title="Edit"><i class="fas fa-fw fa-edit"></i></a>
                              <a href="{{ route('menu.list', $item->slug) }}" class="btn btn-sm btn-primary custom-hover" title="List"><i class="fas fa-fw fa-list"></i></a>

                            </div>
                          </td>
                          <td>{{ $item->name ?? '-' }}</td>
                          <td>{{ $item->description ?? '-' }}</td>
                        </tr>
                      @empty
                        <tr>
                          <td colspan="3" class="text-center">Belum Ada Data</td>
                        </tr>
                      @endforelse
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
@endsection