@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row my-2">
        <div class="col-sm-12">
          <a href="{{ route('menu.index') }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h4 class="m-0 text-dark">
            Tambah Menu Dokumen SPMI
          </h4>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  <!-- Main content -->
    <div class="content">
      <div class="row">
        <!-- /.col-md-6 -->
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post" action="{{ route('menu.store') }}">
                @csrf
                <div class="form-group">
                  <label for="">Nama Menu Dokumen</label>
                  <input type="text" name="name" id="" class="form-control form-control-sm" placeholder="Input nama menu dokumen" value="{{ old('name') }}" required autofocus>
                  @error('name')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Deskripsi</label>
                  <textarea name="description" class="form-control form-control-sm" placeholder="Input deskripsi" rows="5">{{ old('description') }}</textarea>
                  @error('description')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>

                <div class="form-group row">
                  <button class="btn btn-block btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->
  </div>
@endsection