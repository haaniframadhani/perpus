<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <title>Sistem Penjamin Mutu Internal</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/fontawesome-free/css/all.min.css') }}">

  @yield('styles')

  <!-- Toastr -->
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/toastr/toastr.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin-lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-primary navbar-dark">
    <div class="container">
      
      <a href="{{ route('home') }}" class="navbar-brand">
        {{-- <img src="{{ asset('admin-lte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
        <span class="brand-text">LP3I</span>
      </a>

      <button class="navbar-toggler order-2 btn-sm" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <small>
          <span class="navbar-toggler-icon"></span>
        </small>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">

        <div class="navbar-nav ml-auto">
          @guest
          <li class="nav-item">
            <a class="nav-link font-weight-bold active" href="{{ route('formulir.list') }}">Formulir</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-light my-2 py-1 my-sm-0" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @else

          <li class="nav-item">
            <a class="nav-link font-weight-bold active" href="{{ route('menu.index') }}">Menu</a>
          </li>

          <li class="nav-item">
            <a class="nav-link font-weight-bold active" href="{{ route('formulir.index') }}">Formulir</a>
          </li>

          <li class="nav-item ml-4">
            <div class="btn-group ">
              <a href="#" class="dropdown-hover dropdown-icon" data-toggle="dropdown" style="text-decoration: none; color: #333;">
                <img src="{{ asset('img/default.jpg') }}" class="img-circle mx-2" width="35px">
              </a>
                <div class="dropdown-menu" role="menu" style="font-size:0.85rem; width: 200px;">
                  <div  class="my-2">
                    <img src="{{ asset('img/default.jpg') }}" class="img-circle mx-2" width="30px">
                    <span style="font-size:0.9rem">{{ auth()->user()->name }}</span>
                  </div>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('menu.index') }}">Admin Area</a>

                  <div class="dropdown-divider"></div>

                  <a class="dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                  </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
                </form>
              </div>
            </div>
          </li>
          @endguest
          </div>
      </div>
      
    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- Default to the left -->
    <strong>LP3I All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/ujs.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

@yield('scripts')

<!-- Toastr -->
<script src="{{ asset('admin-lte/plugins/toastr/toastr.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>



<script>
  @if(Session::has('message'))
    toastr.{{Session::get('alert')}}('{{ Session::get('message') }}')
    @php
      Session::forget('message')
    @endphp
  @endif
</script>
</body>
</html>
