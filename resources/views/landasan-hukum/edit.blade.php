@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row my-2">
        <div class="col-sm-12">
          <a href="{{ route('menu.list', $menu->slug) }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h4 class="m-0 text-dark">
            Edit Landasan Hukum
          </h4>
        </div><!-- /.col -->
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('menu.list', $menu->slug) }}">{{ $menu->name }}</a></li>
            <li class="breadcrumb-item active">Landasan Hukum</li>
          </ol>
        </div>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  <!-- Main content -->
    <div class="content">
      <div class="row">
        <!-- /.col-md-6 -->
        <div class="col-md-12">
          <div class="card card-primary card-outline shadow">
            <div class="card-body">
              <form method="post" action="{{ route('menu.landasan-hukum.update', ['slug'=>$slug, 'id'=>$data->id]) }}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                  <label for="">Tahun</label>
                  <input type="number" name="year" class="form-control form-control-sm" placeholder="Input tahun" value="{{ old('year') ?? $data->year }}" required autofocus>
                  @error('year')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Dasar Hukum</label>
                  <textarea name="name" class="form-control form-control-sm" placeholder="Input dasar hukum" required>{{ old('name') ?? $data->name }}</textarea>
                  @error('name')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Dokumen</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="hidden" name="old_file" class="custom-file-input" value="{{ $data->file ?? null }}">
                      <input type="file" name="document" class="custom-file-input" id="exampleInputFile">
                      <label class="custom-file-label" for="exampleInputFile">{{ $data->file ?? 'Choose file' }}</label>
                    </div>
                  </div>
                  @error('document')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>

                <div class="form-group row">
                  <button class="btn btn-block btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init();
    });
  </script>
@endsection