@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row my-2">
        <div class="col-sm-12">
          <a href="{{ route('formulir.index') }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h4 class="m-0 text-dark">
            Edit Formulir
          </h4>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  <!-- Main content -->
    <div class="content">
      <div class="row">
        <!-- /.col-md-6 -->
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post" action="{{ route('formulir.update', $data->id) }}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                  <label for="">Menu</label>
                  <select name="menu" class="form-control form-control-sm select-menu" data-placeholder="Pilih Menu" style="width:100%">
                    @if ($data->sub_menu_id)
                      <option value="{{ $data->subMenu->menu_id }}">{{ $data->subMenu->menu->name ?? '-' }}</option>
                    @else
                      <option value=""></option>
                    @endif  
                  </select>
                  <input type="hidden" name="menu_name" value="{{ old('menu_name') }}">
                </div>
                <div class="form-group">
                  <label for="">Sub Menu</label>
                  <select name="sub_menu" class="select2 form-control form-control-sm select-sub-menu " data-placeholder="Pilih Sub Menu" style="width:100%">
                    @if ($data->sub_menu_id)
                      <option value="{{ $data->sub_menu_id }}">{{ $data->subMenu->name ?? '-' }}</option>
                    @else
                      <option value=""></option>
                    @endif  
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Nomor</label>
                  <input type="text" name="kode" id="" class="form-control form-control-sm" placeholder="Input nomor formulir" value="{{ old('kode') ?? $data->kode }}" required autofocus>
                  @error('kode')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Nama Formulir</label>
                  <input type="text" name="name" id="" class="form-control form-control-sm" placeholder="Input nama formulir" value="{{ old('name') ?? $data->name }}" required>
                  @error('name')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Dokumen</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="hidden" name="old_file" class="custom-file-input" value="{{ $data->file ?? null }}">
                      <input type="file" name="document" class="custom-file-input" id="exampleInputFile">
                      <label class="custom-file-label" for="exampleInputFile">{{ $data->file ?? 'Choose file' }}</label>
                    </div>
                  </div>
                  @error('document')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label>Izinkan Download?</label>
                  <div class="custom-control custom-switch">
                    @if (old('is_allowed'))
                      <input type="checkbox" name="is_allowed" class="custom-control-input is-allowed" id="customSwitch1" {{ old('is_allowed') ? "checked" : null }}>
                    @else
                      <input type="checkbox" name="is_allowed" class="custom-control-input is-allowed" id="customSwitch1" {{ $data->is_allowed ? "checked" : null }}>
                    @endif
                    <label class="custom-control-label is-allowed-label" for="customSwitch1"></label>
                  </div>
                </div>

                <div class="form-group row">
                  <button class="btn btn-block btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->
  </div>
@endsection

@section("styles")
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/select2/css/select2.css') }}">
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <style>
    .select2-container--bootstrap4 .select2-selection__clear {
      width: 1.1em;
      height: 1.1em;
      line-height: 1.15em;
      padding-left: 0.3em;
      margin-top: 0.5em;
      border-radius: 100%;
      background-color: #e22;
      color: #fff;
      float: right;
      margin-right: 0.3em;
    }
  </style>
@endsection

@section('scripts')
  <script src="{{ asset('admin-lte/plugins/select2/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
  <script src="{{ asset('js/custom-select2.js') }}"></script>
  <script>
    $(document).ready(function () {
      $(".select2").select2({
        theme: 'bootstrap4',
        allowClear:true,
      });

      customSelect2("-- Pilih Menu --", $(".select-menu"), "{{ route('resource.menu') }}");

      $(".select-menu").on('change', function () {
        var menu = {
          menu_id : $(this).find('option:selected').val()
        };

        $(".select-sub-menu").val('');
        customSelect2WithParameter("-- Pilih Sub Menu --", $(".select-sub-menu"), menu, "{{ route('resource.sub-menu') }}");
      }).trigger('load')

      $("select-menu").on('select2:select', function(e) {
        $("input[name=menu_name]").val(e.params.data.text)
      })

      bsCustomFileInput.init();

      $(".is-allowed").on('change', function() {
        var checked = $(this).is(":checked");

        if (checked) {
          $('.is-allowed-label').text('Ya');
        } else {
          $('.is-allowed-label').text('Tidak');
        }
      }).trigger('change');

    });
  </script>
@endsection