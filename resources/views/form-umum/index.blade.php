@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row mt-2">
        <div class="col-sm-12">
          <h4 class="m-0 text-dark">
            {{-- Formulir --}}
          </h4>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="row my-3">
      <div class="col-lg col-md-12">
        <div class="card card-primary card-outline shadow">
          <div class="card-header">
            <div class="card-title">Formulir</div>
          </div>
          <div class="card-body table-responsive">

            @if (auth()->user())
            <div class="row mb-3">
              <div class="col-md-12">
                <a href="{{ route('formulir.create') }}" class="btn btn-primary text-white"><i class="fas fa-plus"></i> Tambah</a>
              </div>
            </div>
            @endif
            
            <div class="row">
              <div class="col-lg col-md-12">
                <table class="table table-hover text-nowrap table-bordered" id="dashboard">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Menu</th>
                      <th>Sub Menu</th>
                      <th>Nomor/Kode Formulir</th>
                      <th>Nama Formulir</th>
                      <th>Izinkan Download</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($data as $item)
                      <tr class="align-middle">
                        <td class="text-center col-2">
                          <div class="btn-group transparent">
                            <a data-method="delete" data-confirm="Anda yakin ingin menghapus data ini?" href="{{ route('formulir.delete', $item->id) }}"class="btn btn-sm btn-primary custom-hover" title="Hapus"><i class="fas fa-fw fa-trash"></i></a>
                            <a href="{{ route('formulir.edit', $item->id) }}" class="btn btn-sm btn-primary custom-hover" title="Edit"><i class="fas fa-fw fa-edit"></i></a>
                          </div>
                        </td>
                        <td>{{ $item->subMenu->menu->name ?? '-' }}</td>
                        <td>{{ $item->subMenu->name ?? '-' }}</td>
                        <td>{{ $item->kode ?? '-' }}</td>
                        <td>
                          @if ($item->is_allowed)
                            <a href="{{ $item->file_url }}" target="_blank">{{ $item->name ?? '-' }}</a>
                          @else
                            {{ $item->name ?? '-' }}
                          @endif
                        </td>
                        <td>{{ $item->is_allowed ? 'Ya' : 'Tidak' }}</td>
                      </tr>
                    @empty
                      <tr>
                        <td colspan="4" class="text-center">Belum Ada Data</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection

@section('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('scripts')
<!-- DataTables -->
  <script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script>
      $("#dashboard").DataTable({
        "responsive": false,
        "autoWidth": true,
      });
  </script>
@endsection