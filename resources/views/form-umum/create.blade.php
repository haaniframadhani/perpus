@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row my-2">
        <div class="col-sm-12">
          <a href="{{ route('formulir.index') }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h4 class="m-0 text-dark">
            Tambah Formulir
          </h4>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  <!-- Main content -->
    <div class="content">
      <div class="row">
        <!-- /.col-md-6 -->
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post" action="{{ route('formulir.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="">Menu</label>
                  <select name="menu" class="form-control form-control-sm select-menu" data-placeholder="Pilih Menu" style="width:100%">
                      <option value=""></option>
                  </select>
                  <input type="hidden" name="menu_name" value="{{ old('menu_name') }}">
                </div>
                <div class="form-group">
                  <label for="">Sub Menu</label>
                  <select name="sub_menu" class="select2 form-control form-control-sm select-sub-menu " data-placeholder="Pilih Sub Menu" style="width:100%">
                      <option value=""></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Nomor</label>
                  <input type="text" name="kode" id="" class="form-control form-control-sm" placeholder="Input nomor formulir" value="{{ old('kode') }}" required autofocus>
                  @error('kode')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Nama Formulir</label>
                  <input type="text" name="name" id="" class="form-control form-control-sm" placeholder="Input nama formulir" value="{{ old('name') }}" required>
                  @error('name')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="">Dokumen</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" name="document" class="custom-file-input" id="exampleInputFile" required>
                      <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                    </div>
                  </div>
                  @error('document')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label>Izinkan Download?</label>
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="is_allowed" class="custom-control-input is-allowed" id="customSwitch1">
                    <label class="custom-control-label is-allowed-label" for="customSwitch1"></label>
                  </div>
                </div>

                <div class="form-group row">
                  <button class="btn btn-block btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->
  </div>
@endsection

@section("styles")
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/select2/css/select2.css') }}">
  <link rel="stylesheet" href="{{ asset('admin-lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <style>
    .select2-container--bootstrap4 .select2-selection__clear {
      width: 1.1em;
      height: 1.1em;
      line-height: 1.15em;
      padding-left: 0.3em;
      margin-top: 0.5em;
      border-radius: 100%;
      background-color: #e22;
      color: #fff;
      float: right;
      margin-right: 0.3em;
    }
  </style>
@endsection

@section('scripts')
  <script src="{{ asset('admin-lte/plugins/select2/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
  <script src="{{ asset('js/custom-select2.js') }}"></script>
  <script>
    $(document).ready(function () {

      customSelect2("-- Pilih Menu --", $(".select-menu"), "{{ route('resource.menu') }}");

      $(".select-menu").on('change', function () {
        var menu = {
          menu_id : $(this).find('option:selected').val()
        };

        $(".select-sub-menu").val('');
        customSelect2WithParameter("-- Pilih Sub Menu --", $(".select-sub-menu"), menu, "{{ route('resource.sub-menu') }}");
      }).trigger('change')

      $("select-menu").on('select2:select', function(e) {
        $("input[name=menu_name]").val(e.params.data.text)
      })

      bsCustomFileInput.init();

      $(".is-allowed").on('change', function() {
        var checked = $(this).is(":checked");

        if (checked) {
          $('.is-allowed-label').text('Ya');
        } else {
          $('.is-allowed-label').text('Tidak');
        }
      }).trigger('change');

    });
  </script>
@endsection