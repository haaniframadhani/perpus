@extends('layouts.master')

@section('content')
<div class="container">
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="row mt-2">
        <div class="col-sm-12">
          <a href="{{ route('home') }}" class="mr-1">
            <i class="fa fa-sm fa-arrow-left"> </i>
            Kembali
          </a>
          <h3 class="m-0 text-dark">
            {{ $menu->name ?? '-' }}
          </h3>
        </div><!-- /.col -->
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Menu</a></li>
            <li class="breadcrumb-item active">{{ $menu->name ?? '-' }}</li>
          </ol>
        </div>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="row my-3">
      <div class="col-lg col-md-12">
        <div class="card card-primary card-outline shadow">
          <div class="card-header">
            <div class="card-title">Landasan Hukum</div>
          </div>
          <div class="card-body table-responsive">
            <div class="row">
              <div class="col-lg col-md-12">
                <table class="table table-hover text-nowrap table-bordered">
                  <thead>
                    <tr>
                      <th>Tahun</th>
                      <th>Dasar Hukum</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($menu->landasanHukum as $item)
                      <tr class="align-middle">
                        <th>{{ $item->year ?? '-' }}</th>
                        <td>
                          <a href="{{ $item->file_url }}" target="_blank">{{ $item->name ?? '-' }}</a>
                        </td>
                      </tr>
                    @empty
                      <tr>
                        <td colspan="3" class="text-center">Belum Ada Data</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="row my-3">
      <div class="col-lg col-md-12">
        <div class="card card-primary card-outline shadow">
          <div class="card-header">
            <div class="card-title">
              Sub Menu
            </div>
          </div>
          <div class="card-body table-responsive">
            <div class="row">
              <div class="col-lg col-md-12">
                <table class="table table-hover text-nowrap table-bordered">
                  <thead>
                    <tr>
                      <th width="50px">No.</th>
                      <th>Dokumen</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($menu->subMenu as $item)
                      <tr class="align-middle">
                        <th class="text-center">
                          {{ $loop->iteration }}
                        </th>
                        <td>
                          <a href="{{ route('formulir', ['menu' => $menu->slug, 'sub' => $item->slug]) }}">{{ $item->name ?? '-' }}</a>
                        </td>
                      </tr>
                    @empty
                      <tr>
                        <td colspan="3" class="text-center">Belum Ada Data</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection