<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\SubMenuController;
use App\Http\Controllers\LandasanHukumController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\FormUmumController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ResourceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function() {
    Route::group(['prefix' => 'menu/', 'as' => 'menu.'], function() {
        Route::get('', [MenuController::class, 'index'])->name('index');
        Route::get('create', [MenuController::class, 'create'])->name('create');
        Route::post('store', [MenuController::class, 'store'])->name('store');
        Route::get('{id}/edit', [MenuController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [MenuController::class, 'update'])->name('update');
        Route::delete('{id}/delete', [MenuController::class, 'delete'])->name('delete');

        Route::get('{slug}/list', [MenuController::class, 'list'])->name('list');

        // Landasan Hukum
        Route::group(['prefix' => '{slug}/landasan-hukum/', 'as' => 'landasan-hukum.'], function() {
            Route::get('create', [LandasanHukumController::class, 'create'])->name('create');
            Route::post('store', [LandasanHukumController::class, 'store'])->name('store');
            Route::get('{id}/edit', [LandasanHukumController::class, 'edit'])->name('edit');
            Route::put('{id}/update', [LandasanHukumController::class, 'update'])->name('update');
            Route::delete('{id}/delete', [LandasanHukumController::class, 'delete'])->name('delete');
        });

        // Sub Menu
        Route::group(['prefix' => '{slug}/sub-menu/', 'as' => 'sub-menu.'], function() {
            Route::get('create', [SubMenuController::class, 'create'])->name('create');
            Route::get('{id}/edit', [SubMenuController::class, 'edit'])->name('edit');
            Route::post('store', [SubMenuController::class, 'store'])->name('store');
            Route::put('{id}/update', [SubMenuController::class, 'update'])->name('update');
            Route::delete('{id}/delete', [SubMenuController::class, 'delete'])->name('delete');

            Route::get('{slug2}/list', [SubMenuController::class, 'list'])->name('list');

            Route::group(['prefix' => '{slug2}/formulir/', 'as' => 'formulir.'], function() {
                Route::get('create', [FormController::class, 'create'])->name('create');
                Route::post('store', [FormController::class, 'store'])->name('store');
                Route::get('{id}/edit', [FormController::class, 'edit'])->name('edit');
                Route::put('{id}/update', [FormController::class, 'update'])->name('update');
                Route::delete('{id}/delete', [FormController::class, 'delete'])->name('delete');
            });
        });
    });

    Route::group(['prefix' => 'formulir/', 'as' => 'formulir.'], function() {
        Route::get('index', [FormUmumController::class, 'index'])->name('index');
        Route::get('create', [FormUmumController::class, 'create'])->name('create');
        Route::post('store', [FormUmumController::class, 'store'])->name('store');
        Route::get('{id}/edit', [FormUmumController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [FormUmumController::class, 'update'])->name('update');
        Route::delete('{id}/delete', [FormUmumController::class, 'delete'])->name('delete');
    });

    Route::group(['prefix'=>'resource/', 'as'=>'resource.'], function() {
        Route::get('menu', [ResourceController::class, 'menu'])->name('menu');
        Route::get('sub-menu', [ResourceController::class, 'subMenu'])->name('sub-menu');
    });
});

// Route::resource('book', 'App\Http\Controllers\BookController');

Auth::routes();

Route::get('formulir/list', [FormUmumController::class, 'list'])->name('formulir.list');

Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('{menu}', [HomeController::class, 'list'])->name('list');
Route::get('{menu}/{sub}', [HomeController::class, 'formulir'])->name('formulir');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');